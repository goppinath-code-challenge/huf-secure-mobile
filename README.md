#  Huf Secure Mobile - Coding Challenge on 10.06.2020

## Important !!!

Here by I acknowledge that all the characters in the source codes are generated and typed by me and it is a 100% work completed by my own.

## Usage of external dependencies

None of the external dependencies are used.

## Assumptions

I assumed that the App's minimum deployment target is iOS 13 and Swift 5.2 and SwiftUI.

## Architecture

 `MVVM` is strictly used.

## Known issues

0. The design is tested only on iPhon 11.
1. No comments used, because code speaks better than words.
2. `RegistrationView` is not implemented.

## Task checklist

0. The original task is given to be refactored, but I took totally a different approach to achieve the result to the given task. During the interview, the interviewer has shown very much interest in SwiftUI. Therefore the task is completed using SwiftUI.

1. Usage `singlton`  is elemninated.
2. Very strong validation logic implemented.
3. Good UI

## Test Log in

E-Mail: login@mail.com
password: 123456
