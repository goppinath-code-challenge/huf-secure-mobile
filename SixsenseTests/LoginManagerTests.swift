//
//  LoginManagerTests.swift
//  SixsenseTests
//
//  Created by Goppinath Thurairajah on 21.06.20.
//  Copyright © 2020 Goppinath Thurairajah. All rights reserved.
//

import XCTest
@testable import Sixsense

class LoginManagerTests: XCTestCase {

    let loginManager = LoginManager()
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testLoginEmailMismatch() throws {
    
        let emailAndRule = LoginManager.ValueAndRule(value: "online@mail.com",
                                                     validationRule: Validation.AndRule(rules: [Validation.NotEmptyRule(),
                                                                                                Validation.EmailRule()]))
        
        let passwordAndRule = LoginManager.ValueAndRule(value: "password",
                                                        validationRule: Validation.AndRule(rules: [Validation.NotEmptyRule(),
                                                                                                   Validation.MinimumLengthRule(minimumLength: 6)]))
        
        loginManager.login(emailAndRule: emailAndRule,
                           passwordAndRule: passwordAndRule) { result in
                            
                            switch result {
                            case .failure(let loginManagerError):
                                
                                XCTAssertEqual(loginManagerError, .emailMismatch)
                                
                            case .success(_): break
                            }
        }
    }
    
    func testLoginPasswordMismatch() throws {
    
        let emailAndRule = LoginManager.ValueAndRule(value: User.default.email,
                                                     validationRule: Validation.AndRule(rules: [Validation.NotEmptyRule(),
                                                                                                Validation.EmailRule()]))
        
        let passwordAndRule = LoginManager.ValueAndRule(value: "password",
                                                        validationRule: Validation.AndRule(rules: [Validation.NotEmptyRule(),
                                                                                                   Validation.MinimumLengthRule(minimumLength: 6)]))
        
        loginManager.login(emailAndRule: emailAndRule,
                           passwordAndRule: passwordAndRule) { result in
                            
                            switch result {
                            case .failure(let loginManagerError):
                                
                                XCTAssertEqual(loginManagerError, .passwordMismatch)
                                
                            case .success(_): break
                            }
        }
    }
}
