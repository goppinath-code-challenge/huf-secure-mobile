//
//  ValidationTests.swift
//  SixsenseTests
//
//  Created by Goppinath Thurairajah on 21.06.20.
//  Copyright © 2020 Goppinath Thurairajah. All rights reserved.
//

import XCTest
@testable import Sixsense

class ValidationTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testNotEmptyRule() throws {
    
        XCTAssertThrowsError(try Validation.NotEmptyRule().validate(value: "")) {
        
            XCTAssertEqual($0 as? ValidationError, .empty)
        }
    }
    
    func testEmailRule() throws {
    
        XCTAssertThrowsError(try Validation.EmailRule().validate(value: "login@")) {
        
            XCTAssertEqual($0 as? ValidationError, .invalidEmail)
        }
    }
    
    func testMinimumLengthRule() throws {
    
        XCTAssertThrowsError(try Validation.MinimumLengthRule(minimumLength: 6).validate(value: "login")) {
        
            XCTAssertEqual($0 as? ValidationError, .invalidMinimumLength(6))
        }
    }
}
