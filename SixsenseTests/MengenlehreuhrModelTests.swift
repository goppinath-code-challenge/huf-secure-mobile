//
//  MengenlehreuhrModelTests.swift
//  SixsenseTests
//
//  Created by Goppinath Thurairajah on 21.06.20.
//  Copyright © 2020 Goppinath Thurairajah. All rights reserved.
//

import XCTest
@testable import Sixsense

class MengenlehreuhrModelTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testFiveFullHoursLights() throws {
    
        guard let date = calendar.date(bySettingHour: 11, minute: 11, second: 0, of: Date()) else { return }
        
        let model = MengenlehreuhrModel(date: date)
        
        XCTAssertEqual(model.fiveFullHoursLights[0].isOn, true)
        XCTAssertEqual(model.fiveFullHoursLights[1].isOn, true)
        XCTAssertEqual(model.fiveFullHoursLights[2].isOn, false)
        XCTAssertEqual(model.fiveFullHoursLights[3].isOn, false)
    }
    
    func testEachFullHourLights() throws {
    
        guard let date = calendar.date(bySettingHour: 11, minute: 11, second: 0, of: Date()) else { return }
        
        let model = MengenlehreuhrModel(date: date)
        
        XCTAssertEqual(model.eachFullHourLights[0].isOn, true)
        XCTAssertEqual(model.eachFullHourLights[1].isOn, false)
        XCTAssertEqual(model.eachFullHourLights[2].isOn, false)
        XCTAssertEqual(model.eachFullHourLights[3].isOn, false)
    }
    
    func testFiveFullMinutesLights() throws {
    
        guard let date = calendar.date(bySettingHour: 11, minute: 17, second: 0, of: Date()) else { return }
        
        let model = MengenlehreuhrModel(date: date)
        
        XCTAssertEqual(model.fiveFullMinutesLights[0].isOn, true)
        XCTAssertEqual(model.fiveFullMinutesLights[1].isOn, true)
        XCTAssertEqual(model.fiveFullMinutesLights[2].isOn, true)
        XCTAssertEqual(model.fiveFullMinutesLights[2].onStateColor, MengenlehreuhrModel.Light.fifteenMinutesColor)
        XCTAssertEqual(model.fiveFullMinutesLights[3].isOn, false)
        XCTAssertEqual(model.fiveFullMinutesLights[4].isOn, false)
        XCTAssertEqual(model.fiveFullMinutesLights[5].isOn, false)
        XCTAssertEqual(model.fiveFullMinutesLights[6].isOn, false)
        XCTAssertEqual(model.fiveFullMinutesLights[7].isOn, false)
        XCTAssertEqual(model.fiveFullMinutesLights[8].isOn, false)
        XCTAssertEqual(model.fiveFullMinutesLights[9].isOn, false)
        XCTAssertEqual(model.fiveFullMinutesLights[10].isOn, false)
    }
    
    func testEachFullMinuteLights() throws {
    
        guard let date = calendar.date(bySettingHour: 11, minute: 11, second: 0, of: Date()) else { return }
        
        let model = MengenlehreuhrModel(date: date)
        
        XCTAssertEqual(model.eachFullMinuteLights[0].isOn, true)
        XCTAssertEqual(model.eachFullMinuteLights[1].isOn,false)
        XCTAssertEqual(model.eachFullMinuteLights[2].isOn, false)
        XCTAssertEqual(model.eachFullMinuteLights[3].isOn, false)
    }
}
