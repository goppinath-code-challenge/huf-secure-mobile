//
//  LoginManager.swift
//  Sixsense
//
//  Created by Goppinath Thurairajah on 15.06.20.
//  Copyright © 2020 Goppinath Thurairajah. All rights reserved.
//

import Foundation

struct User {
    
    // Hard coded test user
    static var `default`: User { User(email: "login@mail.com", password: "123456") }
    
    var email: String
    var password: String
}

enum LoginManagerError: Error, Hashable {
    
    case badInput(emailValidationError: ValidationError?, passwordValidationError: ValidationError?)
    case emailMismatch
    case passwordMismatch
    
    public var localizedDescription: String {
        
        switch self {
        case .badInput:         return "Ungültige Eingabe"
        case .emailMismatch:    return "Unbekannte E-Mail oder Benutzer nicht vorhanden"
        case .passwordMismatch: return "Falsches Passwort"
        }
    }
}

struct LoginManager {
    
    struct ValueAndRule {
        
        let value: String
        let validationRule: ValidationRule
        
        func validate() -> ValidationError? {
            
            do { _ = try validationRule.validate(value: value) }
            catch let error as ValidationError { return error }
            catch { return ValidationError.programmingError }
            
            return nil
        }
    }
    
    // Can be used to compare `User` against the data set either in Database or Cloud
    fileprivate func compare(user: User, completion: (Result<User, LoginManagerError>) -> Void) {
        
        if user.email != User.default.email {
            
            completion(.failure(.emailMismatch))
        }
        else if user.password != User.default.password {
            
            completion(.failure(.passwordMismatch))
        }
        else {
            
            completion(.success(user))
        }
    }
    
    func login(emailAndRule: ValueAndRule, passwordAndRule: ValueAndRule, completion: (Result<User, LoginManagerError>) -> Void) {
        
        let emailValidationError = emailAndRule.validate()
        let passwordValidationError = passwordAndRule.validate()
        
        if emailValidationError == nil && passwordValidationError == nil {
            
            compare(user: User(email: emailAndRule.value, password: passwordAndRule.value), completion: completion)
        }
        else {
            
            completion(.failure(LoginManagerError.badInput(emailValidationError: emailValidationError, passwordValidationError: passwordValidationError)))
        }
    }
}
