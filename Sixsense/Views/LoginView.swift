//
//  LoginView.swift
//  Sixsense
//
//  Created by Goppinath Thurairajah on 11.06.20.
//  Copyright © 2020 Goppinath Thurairajah. All rights reserved.
//

import SwiftUI

struct LoginView: View {
    
    private static let noErrorText = " "
    
    @State private var email = ""
    @State private var password = ""
    
    @State private var emailErrorText: String = noErrorText
    @State private var passwordErrorText: String = noErrorText
    
    @State private var showProfile = false
    
    var body: some View {
        
        VStack(alignment: .leading) {
            
            Spacer().frame(height: 50)
            
            VStack(alignment: .center, spacing: -40) {
                
                Image("six-sense-logo-with-text")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .padding([.leading, .top, .bottom, .trailing])
                
                Text("Security is key".uppercased())
                .font(.system(size: 20, weight: .medium))
                .font(.body)
                .kerning(2)
                .foregroundColor(Color(#colorLiteral(red: 0.6587668061, green: 0.6588612199, blue: 0.6587369442, alpha: 1)))
                .multilineTextAlignment(.center)
                .padding([.leading, .top, .bottom, .trailing])
            }
        
            ValidatableTextField(title: "E-Mail", placeHolder: "", iconImage: UIImage(systemName: "person.circle")!, keyboardType: .emailAddress, text: $email, isSecure: false, errorText: emailErrorText)
                .padding()
                .previewLayout(.fixed(width: 400, height: 100))
            
            ValidatableTextField(title: "Passwort", placeHolder: "", iconImage: UIImage(systemName: "lock.circle")!, keyboardType: .emailAddress, text: $password, isSecure: true, errorText: passwordErrorText)
                .padding()
                .previewLayout(.fixed(width: 400, height: 100))
            
            Spacer()
            
            Button(action: login) {
                
                PrimaryButton(title: "Login", foregroundColor: .white, background: Color.accentColor)
            }
            
            Spacer()
        }
            
        .padding()
        .background(Color("defaultBackgroundColor"))
        .edgesIgnoringSafeArea(.all)
    }
    
    func login() {
        
        let loginManager = LoginManager()
        
        let emailAndRule = LoginManager.ValueAndRule(value: email,
                                                     validationRule: Validation.AndRule(rules: [Validation.NotEmptyRule(),
                                                                                                Validation.EmailRule()]))
        
        let passwordAndRule = LoginManager.ValueAndRule(value: password,
                                                        validationRule: Validation.AndRule(rules: [Validation.NotEmptyRule(),
                                                                                                   Validation.MinimumLengthRule(minimumLength: 6)]))
        loginManager.login(emailAndRule: emailAndRule,
                           passwordAndRule: passwordAndRule) { result in
                            
                            switch result {
                                
                            case .failure(let loginManagerError):
                                
                                switch loginManagerError {
                                case .badInput(let emailValidationError, let asswordValidationError):
                                    
                                    emailErrorText = emailValidationError?.localizedDescription ?? LoginView.noErrorText
                                    passwordErrorText = asswordValidationError?.localizedDescription ?? LoginView.noErrorText
                                    
                                case .emailMismatch:
                                    
                                    emailErrorText = loginManagerError.localizedDescription
                                    passwordErrorText = LoginView.noErrorText
                                    
                                case .passwordMismatch:
                                    
                                    emailErrorText = LoginView.noErrorText
                                    passwordErrorText = loginManagerError.localizedDescription
                                }
                                
                            case .success(let user):
                                
                                UIApplication.window?.rootViewController = UIHostingController(rootView: ProfileView(user: user))
                            }
        }
    }
}

struct loginView_Previews: PreviewProvider {
    static var previews: some View {
        
        Group {
            
            NavigationView {
                
                LoginView()
            }
        }
        
    }
}
