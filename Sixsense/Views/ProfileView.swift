//
//  ProfileView.swift
//  Sixsense
//
//  Created by Goppinath Thurairajah on 16.06.20.
//  Copyright © 2020 Goppinath Thurairajah. All rights reserved.
//

import SwiftUI

struct ProfileView: View {
    
    let user: User
    
    @State var model = MengenlehreuhrModel(date: Date())
    
    @State var showLogoutAlert = false
    
    let combineTimer = CombineTimer(interval: 1)
    
    var body: some View {
        
        VStack(alignment: .center, spacing: 0) {
            
            ZStack {
                
                Rectangle()
                    .foregroundColor(Color("hufRedColor"))
                    .frame(width: nil, height: 220)
                
                VStack(alignment: .center, spacing: 40) {
                    
                    Spacer().frame(width: nil, height: 20)
                    
                    HStack {
                        
                        Text("Email:")
                            .font(.headline)
                            .fontWeight(.semibold)
                            .foregroundColor(Color.white)
                            .multilineTextAlignment(.leading)
                        
                        Text(user.email)
                            .font(.headline)
                            .fontWeight(.light)
                            .foregroundColor(Color.white)
                            .multilineTextAlignment(.leading)
                    }
                    
                    PrimaryButton(title: "Abmelden", foregroundColor: .red, background: .white)
                        .frame(width: 160, height: nil, alignment: .center)
                        .padding()
                        
                        .onTapGesture {
                            
                            self.showLogoutAlert = true
                    }
                    .alert(isPresented: $showLogoutAlert, content: {
                        
                        Alert(title: Text("Hintwise"),
                              message: Text("Wollen Sie sich wirklich abmelden?"),
                              primaryButton: .destructive(Text("Ja"), action: {
                                UIApplication.window?.rootViewController = UIHostingController(rootView: NavigationView { WelcomeView() })
                              }), secondaryButton: .cancel())
                    })
                }
            }
            
            ZStack {
                
                Rectangle()
                    .foregroundColor(Color("defaultBackgroundColor"))
                
                VStack(alignment: .center, spacing: 0) {
                    
                    Spacer().frame(width: nil, height: 50)
                    
                    MengenlehreuhrView(model: model)
                        .onReceive(combineTimer.currentTimePublisher) { newCurrentTime in
                            
                            self.model = MengenlehreuhrModel(date: Date())
                    }
                    
                    Text(model.dateString)
                        .font(.headline)
                        .fontWeight(.medium)
                        .foregroundColor(Color.white)
                        .multilineTextAlignment(.leading)
                    
                    Spacer().frame(width: nil, height: 50)
                }
            }
        }
        .edgesIgnoringSafeArea(.all)
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView(user: User.default)
    }
}
