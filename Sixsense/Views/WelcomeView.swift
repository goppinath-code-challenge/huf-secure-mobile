//
//  WelcomeView.swift
//  Sixsense
//
//  Created by Goppinath Thurairajah on 11.06.20.
//  Copyright © 2020 Goppinath Thurairajah. All rights reserved.
//

import SwiftUI

struct WelcomeView: View {
    
    var body: some View {
        
        VStack(alignment: .center, spacing: 0) {
            
            Spacer()
            
            VStack(alignment: .leading, spacing: -10) {
                
                Text("SixSense")
                    .font(.system(size: 36, weight: .medium))
                    .kerning(4)
                    .frame(maxWidth: .infinity, alignment: .center)
                    .foregroundColor(Color(#colorLiteral(red: 0.9897648692, green: 0.2990583181, blue: 0.2999471426, alpha: 1)))
                    .padding([.leading, .top, .bottom, .trailing])
                
                Rectangle()
                    .frame(height: 1)
                    .foregroundColor(Color(#colorLiteral(red: 0.1980974078, green: 0.2280248404, blue: 0.2620401978, alpha: 1)))
                    .frame(width: 220, height: nil, alignment: .center)
                    .frame(maxWidth: .infinity, alignment: .center)
                
                Text("Der Schlüssel zum Erfolg")
                    .font(.system(size: 20, weight: .regular))
                    .kerning(4)
                    .frame(maxWidth: .infinity, alignment: .center)
                    .font(Font.title.bold())
                    .foregroundColor(Color(#colorLiteral(red: 0.7939354777, green: 0.5089911222, blue: 0.5111624599, alpha: 1)))
                    .padding([.leading, .top, .bottom, .trailing])
            }
            
            Spacer()
            
            VStack(alignment: .center) {
                
                Image("six-sense-logo")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 120, height: 120)
                    .aspectRatio(contentMode: .fit)
                    .padding([.leading, .top, .bottom, .trailing])
                
                Text("""
                    Gestalten Sie mit uns
                    die Mobilität von morgen
                    """)
                    .fontWeight(.light)
                    .kerning(2)
                    .foregroundColor(Color(#colorLiteral(red: 0.6587668061, green: 0.6588612199, blue: 0.6587369442, alpha: 1)))
                    .multilineTextAlignment(.center)
                    .padding([.leading, .top, .bottom, .trailing])
            }
            
            Spacer()
            
            VStack(spacing: 30) {
                
                NavigationLink(destination: LoginView()) {
                    
                    PrimaryButton(title: "Login", foregroundColor: .white, background: .blue)
                }
                
                NavigationLink(destination: EmptyView()) {
                    
                    PrimaryButton(title: "Registrieren", foregroundColor: .blue, background: .white)
                }
            }
            .padding([.leading, .top, .bottom, .trailing])
            
            Spacer()
        }
        .background(Color("defaultBackgroundColor"))
        .edgesIgnoringSafeArea(.all)
    }
}

struct WelcomeView_Previews: PreviewProvider {
    static var previews: some View {
        
        Group {
            
            WelcomeView()
        }
    }
}
