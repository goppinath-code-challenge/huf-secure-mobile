//
//  Validation.swift
//  Sixsense
//
//  Created by Goppinath Thurairajah on 11.06.20.
//  Copyright © 2020 Goppinath Thurairajah. All rights reserved.
//

import Foundation

public enum ValidationError: Error, Hashable {
    
    case programmingError
    case empty
    case invalidEmail
    case invalidMinimumLength(Int)
    case notEqual(String)
    
    public var localizedDescription: String {
        
        switch self {
        case .programmingError:                 return "Unbekannter Fehler"
        case .empty:                            return "Die Eingabe darf nicht leer sein"
        case .invalidEmail:                     return "Ungültige E-Mail Adresse"
        case .invalidMinimumLength(let count):  return "Bitte geben Sie mehr als \(count) Zeichen ein"
        case .notEqual(let message):            return message
        }
    }
}

public struct Validation {
    
}

public protocol ValidationRule {
    
    func validate(value: String) throws -> Bool
}

extension Validation {
    
    public struct NotEmptyRule: ValidationRule {
        
        public init() { }
        
        public func validate(value: String) throws -> Bool {
            
            guard !value.isEmpty else { throw ValidationError.empty }
            
            return true
        }
    }
    
    public struct EmailRule: ValidationRule {
        
        public init() { }
        
        // Source: http://emailregex.com
        
        public func validate(value: String) throws -> Bool {
            
            let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
            
            guard NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: value) else { throw ValidationError.invalidEmail }
            
            return true
        }
    }
    
    public struct MinimumLengthRule: ValidationRule {

        let minimumLength: Int

        public init(minimumLength: Int) {

            self.minimumLength = minimumLength
        }

        public func validate(value: String) throws -> Bool {

            guard value.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count >= minimumLength else { throw ValidationError.invalidMinimumLength(minimumLength) }

            return true
        }
    }
    
    public struct AndRule: ValidationRule {
        
        let rules: [ValidationRule]
        
        public init(rules: [ValidationRule]) {
            
            self.rules = rules
        }
        
        public func validate(value: String) throws -> Bool {
            
            do {
                
                for rule in rules { _ = try rule.validate(value: value) }
                
                return true
            }
            catch { throw error }
        }
    }
}
