//
//  MengenlehreuhrView.swift
//  Sixsense
//
//  Created by Goppinath Thurairajah on 16.06.20.
//  Copyright © 2020 Goppinath Thurairajah. All rights reserved.
//

import SwiftUI

struct MengenlehreuhrView: View {
    
    var model: MengenlehreuhrModel
    
    let borderColor = #colorLiteral(red: 0.5921055079, green: 0.5921911001, blue: 0.5920783877, alpha: 1)
    let cornerRadius: CGFloat = 8
    let lineWidth: CGFloat = 1
    
    var body: some View {
        
        VStack(alignment: .center, spacing: 12) {
            
            Circle()
                .foregroundColor(Color(self.model.secondsLight.color))
                .frame(width: 100, height: 100, alignment: .center)
                .overlay(
                    Circle()
                    .stroke(Color(self.borderColor), lineWidth: self.lineWidth)
            )
            
            HStack(alignment: .center) {
                
                ForEach(0..<model.fiveFullHoursLights.count) { index in
                    
                    RoundedRectangle(cornerRadius: self.cornerRadius)
                        .foregroundColor(Color(self.model.fiveFullHoursLights[index].color))
                        .overlay(
                            RoundedRectangle(cornerRadius: self.cornerRadius)
                                .stroke(Color(self.borderColor), lineWidth: self.lineWidth)
                    )
                }
            }
            
            HStack(alignment: .center) {
                
                ForEach(0..<model.eachFullHourLights.count) { index in
                    
                    RoundedRectangle(cornerRadius: self.cornerRadius)
                        .foregroundColor(Color(self.model.eachFullHourLights[index].color))
                        .overlay(
                            RoundedRectangle(cornerRadius: self.cornerRadius)
                                .stroke(Color(self.borderColor), lineWidth: self.lineWidth)
                    )
                }
            }
            
            HStack(alignment: .center) {
                
                ForEach(0..<model.fiveFullMinutesLights.count) { index in
                    
                    RoundedRectangle(cornerRadius: self.cornerRadius)
                        .foregroundColor(Color(self.model.fiveFullMinutesLights[index].color))
                        .overlay(
                            RoundedRectangle(cornerRadius: self.cornerRadius)
                                .stroke(Color(self.borderColor), lineWidth: self.lineWidth)
                    )
                }
            }
            
            HStack(alignment: .center) {
                
                ForEach(0..<model.eachFullMinuteLights.count) { index in
                    
                    RoundedRectangle(cornerRadius: self.cornerRadius)
                        .foregroundColor(Color(self.model.eachFullMinuteLights[index].color))
                        .overlay(
                            RoundedRectangle(cornerRadius: self.cornerRadius)
                                .stroke(Color(self.borderColor), lineWidth: self.lineWidth)
                    )
                }
            }
        }
        .padding()
    }
}

struct MengenlehreuhrView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        MengenlehreuhrView(model: MengenlehreuhrModel(date: Date()))
    }
}
