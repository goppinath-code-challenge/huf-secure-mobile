//
//  MengenlehreuhrModel.swift
//  Sixsense
//
//  Created by Goppinath Thurairajah on 16.06.20.
//  Copyright © 2020 Goppinath Thurairajah. All rights reserved.
//

import UIKit

let calendar = Calendar.current

struct MengenlehreuhrModel {
    
    let date: Date
    
    fileprivate let hour: Int
    fileprivate let minute: Int
    fileprivate let second: Int
    
    init(date: Date) {
        
        self.date = date
        
        let dateComponents = calendar.dateComponents([.hour, .minute, .second], from: date)
        
        hour = dateComponents.hour ?? 0
        minute = dateComponents.minute ?? 0
        second = dateComponents.second ?? 0
    }
    
    var dateString: String {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .medium
        
        return dateFormatter.string(from: date)
    }
    
    var secondsLight: Light {
        
        Light.seconds(isOn: second % 2 == 0)
    }
    
    var fiveFullHoursLights: [Light] {
        
        var lights = [Light]()
        
        let numberOfLightsToLit = hour / 5
        
        (0..<4).forEach {
            
            lights.append(Light.hours(isOn: $0 < numberOfLightsToLit))
        }

        return lights
    }
    
    var eachFullHourLights: [Light] {
        
        var lights = [Light]()
        
        let numberOfLightsToLit = hour % 5
        
        (0..<4).forEach {
            
            lights.append(Light.hours(isOn: $0 < numberOfLightsToLit))
        }

        return lights
    }
    
    var fiveFullMinutesLights: [Light] {
        
        var lights = [Light]()
        
        let numberOfLightsToLit = minute / 5
        
        (0..<11).forEach {
            
            lights.append(($0 + 1) % 3 == 0 ? Light.fifteenMinutes(isOn: $0 < numberOfLightsToLit) : Light.minutes(isOn: $0 < numberOfLightsToLit))
        }

        return lights
    }
    
    var eachFullMinuteLights: [Light] {
        
        var lights = [Light]()
        
        let numberOfLightsToLit = minute % 5
        
        (0..<4).forEach {
            
            lights.append(Light.minutes(isOn: $0 < numberOfLightsToLit))
        }

        return lights
    }
}

extension MengenlehreuhrModel {
    
    struct Light {
        
        var onStateColor: UIColor
        var offStateColor = UIColor.darkGray
        
        static let secondsColor = UIColor.yellow
        static let hoursColor = UIColor.red
        static let minutesColor = UIColor.yellow
        static let fifteenMinutesColor = UIColor.red
        
        var isOn = false
        
        var color: UIColor {
            
            return isOn ? onStateColor : offStateColor
        }
        
        static func seconds(isOn: Bool) -> Light {
            
            Light(onStateColor: secondsColor, isOn: isOn)
        }
        
        static func hours(isOn: Bool) -> Light {
            
            Light(onStateColor: hoursColor, isOn: isOn)
        }
        
        static func minutes(isOn: Bool) -> Light {
            
            Light(onStateColor: minutesColor, isOn: isOn)
        }
        
        static func fifteenMinutes(isOn: Bool) -> Light {
            
            Light(onStateColor: fifteenMinutesColor, isOn: isOn)
        }
    }
}
