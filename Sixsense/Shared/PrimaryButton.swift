//
//  PrimaryButton.swift
//  Sixsense
//
//  Created by Goppinath Thurairajah on 11.06.20.
//  Copyright © 2020 Goppinath Thurairajah. All rights reserved.
//

import SwiftUI

struct PrimaryButton: View {
    
    let title: String
    let foregroundColor: Color
    let background: Color
    
    var body: some View {
        
        Text(title.uppercased())
            .fontWeight(.bold)
            .foregroundColor(foregroundColor)
            .padding()
            .frame(maxWidth: .infinity)
            .background(background)
            .cornerRadius(5)
            .shadow(radius: 15)
    }
}

struct ButtonViews_Previews: PreviewProvider {
    static var previews: some View {
        
        Group {
            
            PrimaryButton(title: "Log In", foregroundColor: .white, background: Color.accentColor)
                .previewLayout(.fixed(width: 300, height: 100))
            
            PrimaryButton(title: "Sign Up", foregroundColor: Color.accentColor, background: .white)
                .previewLayout(.fixed(width: 300, height: 100))
        }
    }
}
