//
//  ValidatableTextField.swift
//  Sixsense
//
//  Created by Goppinath Thurairajah on 11.06.20.
//  Copyright © 2020 Goppinath Thurairajah. All rights reserved.
//

import SwiftUI

struct ValidatableTextField: View {
    
    let title: String
    let placeHolder: String
    let iconImage: UIImage
    let keyboardType: UIKeyboardType
    let text: Binding<String>
    let isSecure: Bool
    
    var errorText: String
    
    var body: some View {
        
        VStack(alignment: .leading) {
            
            Text(title)
                .fontWeight(.bold)
                .foregroundColor(Color(#colorLiteral(red: 0.370555222, green: 0.3705646992, blue: 0.3705595732, alpha: 1)))
            
            HStack {
                
                if isSecure {
                    
                    SecureField(placeHolder, text: text)
                        .keyboardType(keyboardType)
                        .autocapitalization(.none)
                        .foregroundColor(.white)
                }
                else {
                    
                    TextField(placeHolder, text: text)
                    .keyboardType(keyboardType)
                    .autocapitalization(.none)
                    .foregroundColor(.white)
                }
                
                Image(uiImage: iconImage)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 24, height: 24)
            }
            
            Rectangle()
                .frame(height: 2)
                .foregroundColor(errorText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty ? Color(#colorLiteral(red: 0.370555222, green: 0.3705646992, blue: 0.3705595732, alpha: 1)) : .red)
            
            Text(errorText)
                .font(Font.footnote)
                .fontWeight(.thin)
                .foregroundColor(.red)
                .frame(maxWidth: .infinity, alignment: .trailing)
                .foregroundColor(Color(.lightGray))
        }
    }
}

struct ValidatableTextField_Previews: PreviewProvider {
    static var previews: some View {
        
        Group {
            
            ValidatableTextField(title: "Email", placeHolder: "test@email.com", iconImage: UIImage(systemName: "person.circle")!, keyboardType: .emailAddress, text: .constant(""), isSecure: false, errorText: "Goppi")
                .padding()
                .previewLayout(.fixed(width: 400, height: 100))
            
            ValidatableTextField(title: "Password", placeHolder: "test@email.com", iconImage: UIImage(systemName: "lock.circle")!, keyboardType: .emailAddress, text: .constant(""), isSecure: true, errorText: "")
                .padding()
                .previewLayout(.fixed(width: 400, height: 100))
        }
    }
}
