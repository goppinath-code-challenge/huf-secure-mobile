//
//  UIApplication+Window.swift
//  Sixsense
//
//  Created by Goppinath Thurairajah on 21.06.20.
//  Copyright © 2020 Goppinath Thurairajah. All rights reserved.
//

import UIKit

extension UIApplication {
    
    static var window: UIWindow? {
        
        guard
            let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
            let sceneDelegate = windowScene.delegate as? SceneDelegate,
            let window = sceneDelegate.window
            else { return nil }
        
        return window
    }
}
