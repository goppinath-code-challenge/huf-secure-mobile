//
//  CombineTimer.swift
//  Sixsense
//
//  Created by Goppinath Thurairajah on 21.06.20.
//  Copyright © 2020 Goppinath Thurairajah. All rights reserved.
//

import Foundation

import Combine

class CombineTimer {
    
    lazy var currentTimePublisher = Timer.TimerPublisher(interval: interval, runLoop: .main, mode: .default)
    
    private var cancellable: AnyCancellable?

    private var interval: TimeInterval
    
    init(interval: TimeInterval) {
        
        self.interval = interval
        
        self.cancellable = currentTimePublisher.connect() as? AnyCancellable
    }

    deinit {
        
        self.cancellable?.cancel()
    }
}
